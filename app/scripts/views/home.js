var HomeView = Backbone.Marionette.LayoutView.extend({

  template: 'home',

  regions: {
    list: '#list'
  },

  events: {
    'keydown input.primary': 'submit',
    'click #list li': 'openItem'
  },

  initialize: function () {

  },

  onRender: function () {
    console.log('Homeview rendered');
    this.list.show(new ListView({collection: this.collection}));

    $('body').addClass('slide-1');
/*

    setTimeout(function () {
      $('body').addClass('slide-2');
    }, 11000);
*/


  },
  openItem: function (event) {
    $('#list li', this.$el).removeClass('open');
    $(event.currentTarget).addClass('open');

  },
  submit: function (event) {
    var view = this;
    if (event.keyCode == 13) {
      console.log('submit');
      var newTodo = new ToDo({title: $(event.currentTarget).val(), isNew: true});
      this.list.currentView.collection.add(newTodo);

      $(event.currentTarget).val('');

    }

  }
});


var ListItemView = Backbone.Marionette.ItemView.extend({
  template: 'list-item',
  tagName: 'li',
  initialize: function () {
  },
  onRender: function () {

    this.$el.hide();
    var isNew = this.model.get('isNew');
    if (isNew) {
      this.$el.fadeIn(50);
    }
    else {
      this.$el.show();
    }

  }
});

var ListView = Backbone.Marionette.CollectionView.extend({
  childView: ListItemView,
  tagName: 'ul',
  initialize: function () {
    this.listenTo(this.collection, 'sync', this.render);
  },

});
