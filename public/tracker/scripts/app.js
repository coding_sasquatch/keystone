var app;
$ = jQuery;

Marionette.TemplateCache.prototype.loadTemplate = function(templateId) {
  return Handlebars.templates[templateId];
}

Marionette.TemplateCache.prototype.compileTemplate = function(rawTemplate) {
  return rawTemplate;
}

app = new Backbone.Marionette.Application();

app.addRegions({
  navbar: new Backbone.Marionette.Region({el: '#navbar'}),
  main: new Backbone.Marionette.Region({el: '#main-content'}),
  modal: new Backbone.Marionette.Region({el: '#modal'}),
});

app.addInitializer(function () {

  // Start history
  Backbone.history.start({
    pushState: true,
    root: '/tracker'
  });

  $(document.body).on('click', 'a', function(event) {
    var target = event.currentTarget;

    if (!($(event.currentTarget).hasClass('external'))) {

      var pathname = target.pathname;
      Backbone.history.navigate(pathname, {trigger: true});
      event.preventDefault();
    }

  });

});