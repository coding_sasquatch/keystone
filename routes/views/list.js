var keystone = require('keystone'),
  Post = keystone.list('Post');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res),
		locals = res.locals;

	locals.data = {
		posts: [],
	};

	// Load posts
	view.on('init', function(next) {

		var q = Post.model.find().where('state', 'published').sort('-publishedDate').populate('author').limit('4');
		q.exec(function(err, results) {
			locals.data.posts = results;
			next(err);
		});

    var results = {title: "my title"};
    locals.data.posts = results;

	});

	view.render('lists');
}