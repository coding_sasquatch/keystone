(function() {
var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['home'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<!-- Primary input for adding items -->\n<input type=\"text\" class=\"form-control primary\" placeholder=\"What do you need to do?\"/>\n\n<!-- Element contining collection view -->\n<div id=\"list\">\n</div>";
},"useData":true});

templates['list-item'] = template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"item-wrapper\">\n  <header>\n    "
    + this.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\n  </header>\n\n  <div class=\"details\">\n    <label>Notes</label>\n    <textarea class=\"form-control\"></textarea>\n  </div>\n</div>\n";
},"useData":true});
}());