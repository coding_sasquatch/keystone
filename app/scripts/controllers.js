var HomeController = {

  show: function () {

    var list = new ToDoList();

    list.add(new ToDo({title: 'Make a keystone app'}));
    list.add(new ToDo({title: 'Add some items'}));
    list.add(new ToDo({title: 'Exercise more'}));
    list.add(new ToDo({title: 'Walk the dog'}));
    list.add(new ToDo({title: 'Groceries'}));

    app.main.show(new HomeView({collection: list}));

  }

};