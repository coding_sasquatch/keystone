var app;
$ = jQuery;

Marionette.TemplateCache.prototype.loadTemplate = function(templateId) {
  return Handlebars.templates[templateId];
}

Marionette.TemplateCache.prototype.compileTemplate = function(rawTemplate) {
  return rawTemplate;
}

app = new Backbone.Marionette.Application();

app.addRegions({
  main: new Backbone.Marionette.Region({el: 'main'}),
});

app.addInitializer(function () {

  // Start history
  Backbone.history.start({
    pushState: true,
    root: '/app'
  });

  $(document.body).on('click', 'a', function(event) {
    var target = event.currentTarget;

    if (!($(event.currentTarget).hasClass('external'))) {

      var pathname = target.pathname;
      Backbone.history.navigate(pathname, {trigger: true});
      event.preventDefault();
    }

  });

});

Handlebars.registerHelper('debug', function() {
  console.log(this);
});

app.start();