var keystone = require('keystone');
var engine = require('express3-handlebars');

keystone.init({

  'name': "Keystone",

  'favicon': 'public/favicon.ico',
  'less': 'public',
  'static': ['public'],

  'views': 'views',

  'custom engine': engine({
    layoutsDir: 'views/layouts',
    defaultLayout: 'default',
    helpers: {
      debug: function() {
        console.log(this);
      }
    }
  }),

  'view engine': 'handlebars',

  'auto update': true,
  'mongo': 'mongodb://localhost/keystone',

  'session': true,
  'auth': true,
  'user model': 'User',
  'cookie secret': 'n48Uj1lTY1t273XMH3hbVwmWP7W1MaXV'

});

require('./models');

keystone.set('routes', require('./routes'));

keystone.set('nav', {
	'content': ['posts', 'lists'],
	'users': ['users'],
});

keystone.start();