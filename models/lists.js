var keystone = require('keystone');
var Types = keystone.Field.Types;

var List = new keystone.List('List', {
  autokey: { from: 'title', path: 'key', unique: true },
  label: 'To-Do List'
});

List.add({
  // Add fields for Lists.
  title: { type: String, required: true, initial: true },
  author: { type: Types.Relationship, ref: 'User', index: true },
	publishedDate: { type: Types.Date, index: true },
	type: { initial: true, type: Types.Select, options: ['Grocery', 'To-Do', 'Honey-Do']}
});

List.defaultColumns = 'title, type';
List.register();