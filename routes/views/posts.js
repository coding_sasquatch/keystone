var keystone = require('keystone'),
	Post = keystone.list('Post');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res),
		locals = res.locals;

	// Init locals
	locals.data = {
		posts: []
	};

	// Load posts
	view.on('init', function(next) {

		var q = Post.model.find(function(err, posts) {
  		if (err) return console.log(err);
		});

		q.exec(function(err, results) {
			locals.data.posts = results;
			next(err);
		});

	});

	// Render the view
	view.render('posts');

}
