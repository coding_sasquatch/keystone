var keystone = require('keystone'),
    middleware = require('./middleware'),
    importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initErrorHandlers);
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Handle 404 errors
keystone.set('404', function(req, res, next) {
    res.notfound();
});

keystone.pre('routes', function(req, res, next) {
	res.locals.navLinks = [
		{ label: 'Home', key: 'home', href: '/' },
		{ label: 'Posts', key: 'posts', href: '/posts' },
	];

	res.locals.user = req.user;

	next();

});

// Handle other errors
/*
keystone.set('500', function(err, req, res, next) {
    var title, message;
    if (err instanceof Error) {
        message = err.message;
        err = err.stack;
    }
    res.err(err, title, message);
});
*/

// Load Routes
var routes = {
    views: importRoutes('./views')
    //@todo: practice adding routes
};

// Bind Routes
exports = module.exports = function(app) {

    app.get('/', routes.views.index);
    app.get('/list', routes.views.list);
  	app.all('/posts', routes.views.posts);
  	app.get('/posts/:post', routes.views.post);

}