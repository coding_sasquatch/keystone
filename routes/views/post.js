var keystone = require('keystone');
  var Post = keystone.list('Post');
  var User = keystone.list('User');
exports = module.exports = function(req, res) {

  var view = new keystone.View(req, res),
    locals = res.locals;

  locals.data = {
    post: [],
    author: '',
  };

	// Load the current post
	view.on('init', function(next) {

		var q = Post.model.findOne({
			_id: req.params.post,
		});

		q.exec(function(err, result) {

  		locals.data.post = result;

			var user = User.model.findOne({ _id: result.author });

			user.exec(function(err, author) {
        locals.data.author = author.name;
        locals.data.post.author = author.name;
			});

			next(err);
		});

	});

	view.render('post');

};